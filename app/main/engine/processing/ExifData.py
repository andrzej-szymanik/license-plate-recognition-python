from exif import Image
from datetime import datetime
from babel.dates import format_date, format_time

class ExifData:
    def __init__(self, image):
        imagedata = Image(image)
        if imagedata.has_exif:
            if hasattr(imagedata, 'make') and hasattr(imagedata, 'model'):
                if imagedata.make in imagedata.model:
                    self.camera = imagedata.model
                else:
                    self.camera = imagedata.make + ' ' + imagedata.model

            if hasattr(imagedata, 'datetime'):
                self.datetime = datetime.strptime(imagedata.datetime, '%Y:%m:%d %H:%M:%S')
            elif hasattr(imagedata, 'datetime_digitized'):
                self.datetime = datetime.strptime(imagedata.datetime_digitized, '%Y:%m:%d %H:%M:%S')

            if hasattr(imagedata, 'gps_latitude'):
                self.latitude = imagedata.gps_latitude
                self.latitude_ref = imagedata.gps_latitude_ref
                self.longitude = imagedata.gps_longitude
                self.longitude_ref = imagedata.gps_longitude_ref

    def get_camera(self):
        if hasattr(self, 'camera'):
            return str(self.camera)
        else:
            return 'nieznany'
    
    def get_date(self):
        if hasattr(self, 'datetime'):
            return format_date(self.datetime, format='full', locale='pl_PL')
        else:
            return 'nieznana'

    def get_time(self):
        if hasattr(self, 'datetime'):
            return format_time(self.datetime, "H:mm", locale='pl_PL')
        else:
            return 'nieznany'
    
    def get_print_coords(self):
        if hasattr(self, 'latitude'):
            return str(int(self.latitude[0])) + '°' + str(int(self.latitude[1])) + "'" + str(int(self.latitude[2])) + '"' + self.latitude_ref +\
                ', ' + str(int(self.longitude[0])) + '°' + str(int(self.longitude[1])) + "'" + str(int(self.longitude[2])) + '"' + self.longitude_ref
        else:
            return 'nieznane'

    def get_coords(self):
        if hasattr(self, 'latitude'):
            latitude = int(self.latitude[0]) + int(self.latitude[1])/60 + int(self.latitude[2])/3600
            longitude = int(self.longitude[0]) + int(self.longitude[1])/60 + int(self.longitude[2])/3600

            if self.latitude_ref == 'S':
                latitude *= -1
            if self.longitude_ref == 'W':
                longitude *= -1
            return latitude, longitude
        else:
            return 0, 0