import csv
import base64
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO

class PlateProcessing:
    def __init__(self, plate_number):
        with open('app/static/tablice.csv', mode='r') as infile:
            reader = csv.reader(infile)
            self.data_dictionary = {rows[0]:[rows[1], rows[2], rows[4], rows[3]] for rows in reader}

        self.plate_number = plate_number
        
        if (self.plate_number[2].isnumeric()):
            self.symbol = self.plate_number[:2]
            self.number = self.plate_number[2:]
        else:
            self.symbol = self.plate_number[:3]
            self.number = self.plate_number[3:]
    
    def get_country(self):
        return 'Polska'

    def get_plate_type(self):
        if self.symbol in self.data_dictionary:
            return int(self.data_dictionary[self.symbol][2])
        else:
            return -1
    
    def get_voivodeship(self):
        return self.data_dictionary[self.symbol][0]

    def get_county(self):
        return self.data_dictionary[self.symbol][1]

    def get_feature_id(self):
        return int(self.data_dictionary[self.symbol][3])
    
    def get_render(self):
        msg = self.symbol+' '+self.number

        if len(msg) == 9:
            fnt = ImageFont.truetype('app/static/arklatrs.ttf', 190)
        else:
            fnt = ImageFont.truetype('app/static/arklatrs.ttf', 203)

        image = Image.open('app/static/plate_template.png')
        draw = ImageDraw.Draw(image)
        w, h = draw.textsize(msg, font=fnt)
        W, H = (1120,200)

        draw.text(((W-w)/2,(H-h)/2), msg, font=fnt, fill="black")

        buffered = BytesIO()
        image.save(buffered, format="PNG")
        img_str = "data:image/jpg;base64," + base64.b64encode(buffered.getvalue()).decode('ascii')
        
        return img_str
