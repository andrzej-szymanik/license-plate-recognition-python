import glob

from keras import Model, Input
from keras.applications import MobileNetV2
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers import AveragePooling2D, Flatten, Dense, Dropout
from keras.optimizers import Adam
from keras.utils import to_categorical
from keras_preprocessing.image import load_img, img_to_array, np, os, ImageDataGenerator
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

dataset_paths = glob.glob("dataset_characters/**/*.jpg")

X=[]
labels=[]
for image_path in dataset_paths:
    label = image_path.split(os.path.sep)[-2]
    image=load_img(image_path, target_size=(80,80, 3))
    image=img_to_array(image)
    X.append(image)
    labels.append(label)

X = np.array(X,dtype="float16")
labels = np.array(labels)

lb = LabelEncoder()
lb.fit(labels)
labels = lb.transform(labels)
y = to_categorical(labels)

np.save('license_character_classes.npy', lb.classes_)

# split 10% of data as validation set
(trainX, testX, trainY, testY) = train_test_split(X, y, test_size=0.10, stratify=y, random_state=42)


image_gen = ImageDataGenerator(rotation_range=10,
                              rescale=1/255,
                              width_shift_range=0.1,
                              height_shift_range=0.1,
                              shear_range=0.1,
                              zoom_range=0.1,
                              fill_mode="nearest"
                              )


def create_model():
    baseModel = MobileNetV2(weights="imagenet",include_top=False,input_tensor=Input(shape=(80, 80, 3)))
    headModel = baseModel.output
    headModel = AveragePooling2D(pool_size=(3, 3))(headModel)
    headModel = Flatten(name="flatten")(headModel)
    headModel = Dense(128, activation="relu")(headModel)
    headModel = Dropout(0.5)(headModel)
    headModel = Dense(y.shape[1], activation="softmax")(headModel)
    model = Model(inputs=baseModel.input, outputs=headModel)
    for layer in baseModel.layers:
        layer.trainable = True
    optimizer = Adam(1e-4,1e-4/25)
    model.compile(loss="categorical_crossentropy", optimizer=optimizer, metrics=["accuracy"])

    return model

INIT_LR = 0.2
EPOCHS = 30
model = create_model()
BATCH_SIZE = 32

my_checkpointer = [
                EarlyStopping(monitor='val_loss', patience=5, verbose=0),
                ModelCheckpoint(filepath="License_character_recognition.h5", verbose=1, save_weights_only=True)
                ]

result = model.fit(image_gen.flow(trainX, trainY, batch_size=BATCH_SIZE),
                   steps_per_epoch=len(trainX) // BATCH_SIZE,
                   validation_data=(testX, testY),
                   validation_steps=len(testX) // BATCH_SIZE,
                   epochs=EPOCHS,
                   callbacks=my_checkpointer)

model.save('textModel.h5')
model.save_weights('textModelWeights.h5')
model_json = model.to_json()
with open("MobileNets_character_recognition.json", "w") as json_file:
  json_file.write(model_json)
