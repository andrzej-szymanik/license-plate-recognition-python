import cv2
import matplotlib.pyplot as plt
import numpy as np
from app.main.engine.processing.detection.PlateDetectionHelper import detect_plate
from os.path import splitext
from keras.models import model_from_json


def load_network(path):
    try:
        path = splitext(path)[0]
        with open('%s.json' % path, 'r') as json_file:
            model_json = json_file.read()
        model = model_from_json(model_json, custom_objects={})
        model.load_weights('%s.h5' % path)
        print("Loading model successfully...")
        return model
    except Exception as e:
        print(e)




def preprocess_image(image):
    nparr = np.fromstring(image, np.uint8)
    img_np = cv2.imdecode(nparr, flags=1)
    img = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
    img = img / 255
    return img


def get_plate(image_path, max_dim=608, min_dim=350):
    plate_detection_model = load_network("app/main/engine/processing/detection/plate-detection-model.json")
    vehicle_image = preprocess_image(image_path)
    ratio = float(max(vehicle_image.shape[:2])) / min(vehicle_image.shape[:2])
    side = int(ratio * min_dim)
    bound_dim = min(side, max_dim)
    _, plate_img, _, cor = detect_plate(plate_detection_model, vehicle_image, bound_dim, plate_threshold=0.5)
    return plate_img, cor

# test
# image_path = "test.jpg"
# plate_img, cor = get_plate(image_path)
# print("Coordinate of plate in image: \n", cor)
# plt.figure()
# plt.axis(False)
# plt.imshow(plate_img[0])
# plt.show()
