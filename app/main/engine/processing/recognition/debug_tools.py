import cv2
from matplotlib import pyplot as plt
from app.main.engine.processing.detection.PlateDetection import preprocess_image

def draw_box(image_path, cor, thickness=3):
    pts = []
    x_coordinates = cor[0][0]
    y_coordinates = cor[0][1]
    for i in range(4):
        pts.append([int(x_coordinates[i]), int(y_coordinates[i])])

    pts = np.array(pts, np.int32)
    pts = pts.reshape((-1, 1, 2))
    vehicle_image = preprocess_image(image_path)

    cv2.polylines(vehicle_image, [pts], True, (0, 255, 0), thickness)
    return vehicle_image

def visualizeResult(image):
    plt.figure(figsize=(10,5))
    plt.subplot(1,2,1)
    plt.axis(False)
    plt.imshow(preprocess_image(test_image))
    plt.subplot(1,2,2)
    plt.axis(False)
    plt.imshow(LpImg[0])
    plt.show()


