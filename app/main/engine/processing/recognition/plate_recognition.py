import cv2
import numpy as np

from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import glob


def sort_contours(cnts,reverse = False):
    i = 0
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))
    return cnts


def segment_characters(image):
    plate_image = cv2.convertScaleAbs(image, alpha=(255.0))
    gray_img = cv2.cvtColor(plate_image, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray_img, (3, 3), 0)
    cnn = load_model('app/main/engine/processing/recognition/textModel.h5')
    classes = np.load('app/main/engine/processing/recognition/license_character_classes.npy')
    ret, thresh = cv2.threshold(gray_img, 140, 255, 1)
    contours, hierarchy = cv2.findContours(thresh, cv2.CHAIN_APPROX_SIMPLE, cv2.CHAIN_APPROX_SIMPLE)

    test_roi = plate_image.copy()
    crop_characters = []
    digit_w, digit_h = 80, 80
    licensePlate = ""
    for c in sort_contours(contours):
        (x, y, w, h) = cv2.boundingRect(c)
        ratio = h / w
        if 1 <= ratio <= 3.5:
            if h / plate_image.shape[0] >= 0.5:
                cv2.rectangle(test_roi, (x, y), (x + w, y + h), (0, 255, 0), 2)
                curr_num = thresh[y:y + h, x:x + w]
                curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
                _, curr_num = cv2.threshold(curr_num, 220, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                image = cv2.resize(curr_num,(80,80))
                image = np.stack((image,)*3, axis=-1)
                image = np.expand_dims(image, axis=0)
                result = cnn.predict(image)
                max_index = np.argmax(result)
                character = classes[max_index]
                licensePlate = licensePlate + character

    print(licensePlate)
    return licensePlate




