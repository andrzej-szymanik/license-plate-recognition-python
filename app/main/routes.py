import base64
from flask import request, render_template
from app.main import bp
from app.main.forms import FileUploadForm
from app.main.engine.processing.ExifData import ExifData
from app.main.engine.processing.PlateProcessing import PlateProcessing
from app.main.engine.processing.detection.PlateDetection import get_plate
from app.main.engine.processing.recognition.plate_recognition import segment_characters


@bp.route('/', methods=['GET', 'POST'])
def index():
    form = FileUploadForm()
    if form.validate_on_submit():
        # get image from form, put it through processing;
        image = form.image.data
        image_binary = image.read()

        # exif data processing
        try:
            image_data = ExifData(image_binary)
            image_string = "data:image/jpg;base64," + base64.b64encode(image_binary).decode('ascii')
        except ValueError:
            return render_template('image_error.html', title='Zły format obrazu')

        try:
            LpImg,cor = get_plate(image_binary)
            license_plate = segment_characters(LpImg[0])  
        except AssertionError:
            return render_template('detection_fail.html', title='Błąd wykrywania')

        # put plate number to further processing; processing gets other data from csv
        if len(license_plate) < 7:
            return render_template('detection_fail.html', title='Błąd wykrywania')
        else:
            plate_data = PlateProcessing(license_plate)

        if plate_data.get_plate_type() == -1:
            return render_template('detection_fail.html', title='Błąd wykrywania')
        elif plate_data.get_plate_type() == 0:
            plate_column_1 = "Województwo"
            plate_column_2 = "Powiat"
        else:
            plate_column_1 = "Służba"
            plate_column_2 = "Lokalizacja/rodzaj"

        page_data = [image_string, plate_data.get_render(), plate_data.get_country(), \
                plate_data.get_voivodeship(), plate_data.get_county(), image_data.get_camera(), \
                image_data.get_date(), image_data.get_time(), image_data.get_print_coords(), \
                plate_column_1, plate_column_2, plate_data.get_feature_id(), image_data.get_coords()]
        return render_template('results.html', title='Wyniki', page_data=page_data)
    elif request.method == 'GET':
        return render_template('index.html', title='Strona główna', form=form)