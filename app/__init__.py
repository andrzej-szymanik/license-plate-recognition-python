import logging
import os
from logging.handlers import RotatingFileHandler
from flask import Flask, request, current_app
from config import Config
from flask_bootstrap import Bootstrap
from flask_moment import Moment

bootstrap = Bootstrap()
moment = Moment()

# app creation factory
def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    bootstrap.init_app(app)
    moment.init_app(app)

    # main blueprint
    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    # errors blueprint
    from app.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    # logging mechanism
    if not app.debug and not app.testing:
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler('logs/tabliceo.log', maxBytes=10240, backupCount=10)
        file_handler.setFormatter(
            logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('Tabliceo startup')

    return app