// GLOBAL VARS //
var palette;
var layers;

// FUNCTIONS //
// Sets up layers.
function setLayers() {
    layers.forEach(layer => {
        addLayer(layer[0], 'fill', layer[0], layer[1], layer[2], layer[3], layer[4], layer[5], layer[6], layer[11]);
        addLayer(layer[0] + '-l', 'line', layer[0], layer[1], layer[2], layer[7], layer[8], layer[9], layer[10], layer[11]);
    });
}

// Adds a layer based on parameters.
function addLayer(id, type, source, minzoom, maxzoom, matchrules, baseopacity, extpaint, extlayout, isbase) {
    if (type == 'fill') {
        var paint = {
            'fill-color': ['case', ['boolean', ['feature-state', 'clicked'], false], '#eee', ['match', ["id"], 0, '#fff'].concat(matchrules)],
            'fill-opacity': 1
        }
        var layout = {}
        extpaint.forEach(x => {
            paint[x[0]] = x[1]
        });
        extlayout.forEach(x => {
            layout[x[0]] = x[1]
        });
    } else if (type == 'line') {
        var paint = {
            'line-color': "#fff",
            'line-width': 2
        }
        var layout = {
            'line-join': 'round',
            'line-cap': 'round'
        }
        extpaint.forEach(x => {
            paint[x[0]] = x[1]
        });
        extlayout.forEach(x => {
            layout[x[0]] = x[1]
        });
    }

    map.addLayer({
        id: id,
        type: type,
        source: "composite",
        'source-layer': source,
        minzoom: minzoom,
        maxzoom: maxzoom,
        antialias: true,
        paint: paint,
        layout: layout
    })
}

// Sets up the layers with a proper ID coloured.
function colourFeature(id){
    palette = [
        [id], "#1167B1",
        'rgba(17,100,170,0.2)'
    ]
    layers = [
        ['powiaty', 0, 22, palette, 0, [], [], ['#fbb03b'], 0, [['line-width', ['interpolate', ['exponential', 0.5], ['zoom'], 9.5, 0.5, 11, 7.5]]], [], true, 20200101, 20991231]]

    setLayers();
}

// Creates a marker with the picture creation location.
function createMarker(latitude, longitude) {
    var marker = new mapboxgl.Marker()
        .setLngLat([longitude, latitude])
        .addTo(map);
}

// MAP AND MAP NAV
var map = new mapboxgl.Map({
    container: "map",
    style: "http://127.0.0.1:5000/static/mapstyle.json",
    center: [19.4, 52.1],
    minZoom: 4.5,
    zoom: 4.5,
    maxZoom: 8,
    maxBounds: [[8.67, 46.9], [29.23, 56.93]]
});

map.addControl(new mapboxgl.NavigationControl(), 'top-right');

// load the map
map.on('load', function () {
    map.flyTo({
        center: [19, 52],
        essential: true // this animation is considered essential with respect to prefers-reduced-motion
    });
});